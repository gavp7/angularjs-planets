Hi, thanks for taking the time to look at my project. 
I created this project using Angularjs and Bootstrap for some of the design features.

To view the app working, you will need to have node set up on your machine. navigate to the 'Angularjs Planets' folder on the command line and enter 'node server.js'. You should recieve the message 'App listening on port 8080'. You will then be able to view the app on your browser at 'http://localhost:8080'.

I used node.js to build a small server to host the app on. The code for the server is in the server.js file. Most of this code is copied/slightly modified from an old project which I think was probably in turn copied from an online tutorial somewhere so there's nothing overly notable in this file. 

The main bulk of the app which I have built is in the index.html and controller.js files. I also built a 'style.css' file to make some minor changes and improve the look of the app. I tried to get the colors as similar as possible to the image in the spec by copying it into paint and getting the rgb for the colors to convert to hex online.

 The index.html file uses some bootstrap classes for the table/for the navigation buttons. The navigation buttons won't go beyond page 7 or before page 1, as this is the maximum/minimum pages which return a response. I couldnt find a way to dynamically code this so the maximum number being 7 is hard coded and if more pages were added after future releases this might need to be changed manually. 

 All of the logic operations are done in the controller.js file. I tried to keep it MVC and think of the API as the Model. The 'Refresh' function is the only one which pulls data from the api other than the search and film links which are linking to different addresses. 

 The search function uses a different address, and just updates the 'search' field in the URL when text is entered into the search box and returns the results. 

 All of the required fields are sortable by clicking on the header on the table, this can be sorted into ascending and descending by clicking on the field repeatedly. I did this by using the sortby angular function and toggling true and false when the field is clicked. In order to get this working I had to convert the fields to numbers and then set the unknown numbers to 0 as I had quite a bit of trouble trying to sort them and keep them as unknown. So if the number is 0 in any field it's actually unknown.

 The controller.js file also takes the url of the film and pulls the title of the film to take it's place. This means that the 'film' field contains the names of the films instead of the url to the film. I wasn't sure if this was necessary but I think it looks better/is more informative if you're not looking for more info on the films.  

 The app should also be compatible with different browsers/mobile platforms. I have tested it using Chrome and Firefox, however I don't have access to a Mac so I havn't tested it on safari.

If you have any feedback or suggestions on how I could improve this app further or anything I could have done differently then I'd really appreciate it. 
 Thanks again for taking the time to look at my project.
 I had a lot of fun making it! 




