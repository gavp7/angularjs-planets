//controller.js
angular.module('myApp', [])
.controller('swapi', function($scope, $http) {
	$scope.num = 1; //start on page 1
	$scope.orderByField = "";
	$scope.reverse = false;

	$scope.refresh = function(){ //function to retrieve planets from swapi api
	    $http.get('http://swapi.co/api/planets/?page=' + $scope.num). 
	    then(function(response) {
	    	$scope.planets = response.data;
	    	$scope.dataSort();
	    });
	}

    $scope.dataSort = function(){ //function to deal with some of the data 

    	for(var i=0; i<$scope.planets.results.length; i++){ //This 2d Array loops through all the 'films' in each planet and passes it and its position to the 'Title' function to be altered 
    		for(var j=0; j<$scope.planets.results[i].films.length; j++){
    			$scope.Title($scope.planets.results[i].films[j], i, j); 
    		}
    	}

    	for(var i=0; i<$scope.planets.results.length;i++){ //parse numbers from string to int so they can be ordered correctly 
    		$scope.planets.results[i].population = parseInt($scope.planets.results[i].population);
    		$scope.planets.results[i].diameter = parseInt($scope.planets.results[i].diameter);
    		$scope.planets.results[i].rotation_period = parseInt($scope.planets.results[i].rotation_period);
    		$scope.planets.results[i].orbital_period = parseInt($scope.planets.results[i].orbital_period);

    		//if the number is unknown, set it to 0 (so it doesn't mess up the sorting functions)
    		if(isNaN($scope.planets.results[i].population)){
    			$scope.planets.results[i].population = 0;
    		}
    		if(isNaN($scope.planets.results[i].diameter)){
    			$scope.planets.results[i].diameter = 0;
    		}
    		if(isNaN($scope.planets.results[i].rotation_period)){
    			$scope.planets.results[i].rotation_period = 0;
    		}
    		if(isNaN($scope.planets.results[i].orbital_period)){
    			$scope.planets.results[i].orbital_period = 0;
    		}
   		}
   	} 


    $scope.refresh(); //load first page of planets

	$scope.Search = function(input) { //search function 
		if(input == null) { //if the search box is empty, just view the list as normal
			$scope.refresh(); 
		}
		else { //use the search function on the api to find planets with names matching the input  
			$http.get('https://swapi.co/api/planets/?search=' + input). 
			then(function(response) {
				$scope.planets = response.data;
				$scope.dataSort();
			}); 
		}
	}

	$scope.firstPage = function() { //go to first page
		$scope.num = 1; 
		$scope.refresh();
	}

	$scope.lastPage = function() { //go to last page (This is not set dynamically, I couldn't find anywhere which said how many pages there are other than the 'next page' thing, but I think my method is easier to understand)
		$scope.num = 7;
		$scope.refresh();
	}

    $scope.nextPage = function () { //next page
    	$scope.num = $scope.num + 1;
    	$scope.refresh();
    }

    $scope.previousPage = function () { //previous page
    	$scope.num = $scope.num - 1;
    	$scope.refresh();
    }

    $scope.Title = function(film, i, j) { //this function takes a url and its position in the 2d array above and substitutes the url for the name of the associated film
    	$http.get(film). 
    	then(function(response) {
    		$scope.planets.results[i].films[j] = response.data.title;
    	}); 
    }

    $scope.Sort = function(tosort) {
        if($scope.orderByField == tosort){ //if the orderByField value is already the same as tosort, the sort has already occurred and should be reversed
        	if($scope.reverse == true){  //if this is true then the sort has been reversed and should be put back
        		$scope.reverse = false;
        	} else {
        		$scope.reverse = true;
        	}
        }
        else {
        	$scope.orderByField=tosort; //set the orderByField value to tosort so that the index.html knows what to sort by 
        	$scope.reverse = false;
        }
    }
});